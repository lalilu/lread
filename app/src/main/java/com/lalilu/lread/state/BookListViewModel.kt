package com.lalilu.lread.state

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lalilu.lread.entity.LBook

class BookListViewModel : ViewModel() {
    val bookList: MutableLiveData<List<LBook>> = MutableLiveData()
    val spanCount: MutableLiveData<Int> = MutableLiveData(2)

}