package com.lalilu.lread.state

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lalilu.lread.entity.LBook

class ReadViewModel : ViewModel() {
    val lBook: MutableLiveData<LBook> = MutableLiveData()
    val offscreenPageLimit: MutableLiveData<Int> = MutableLiveData(2)

    companion object {
        const val EPUB_FLAG_NCX = 0
        const val EPUB_FLAG_OPF = 1
        const val EPUB_FLAG_CONTENT = 2
    }
}