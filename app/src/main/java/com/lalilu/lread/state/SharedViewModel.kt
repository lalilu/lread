package com.lalilu.lread.state

import androidx.lifecycle.ViewModel
import com.lalilu.lread.request.BookListRequest

class SharedViewModel : ViewModel() {

    val bookList: BookListRequest = BookListRequest()

}