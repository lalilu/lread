package com.lalilu.lread

import android.content.Context
import android.os.Environment
import com.lalilu.lread.utils.StringUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import nl.siegmann.epublib.domain.MediaType
import java.util.logging.Logger

class GlobalConfig private constructor(context: Context) {
    private val logger = Logger.getLogger(this.javaClass.name)
    val contentResolver = context.contentResolver
    val cachePath = context.externalCacheDir
    val picturePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)

    val tempPrefix: String = "temp_"
    var customizeCss: String = ""

    val typeToLoad = arrayOf(
        MediaType("image/png", ".png"),
        MediaType("image/gif", ".gif"),
        MediaType("image/jpeg", ".jpg"),
        MediaType("text/css", ".css")
    )

    init {
        try {
            GlobalScope.launch(Dispatchers.IO) {
                customizeCss = StringUtil.loadAssetToString(context, "style.css")
            }
        } catch (e: Exception) {
            logger.warning(e.message)
        }
    }

    companion object {
        @Volatile
        private var instance: GlobalConfig? = null

        fun getInstance(context: Context?): GlobalConfig {
            instance ?: synchronized(GlobalConfig::class.java) {
                instance = instance ?: GlobalConfig(context!!)
            }
            return instance!!
        }
    }
}