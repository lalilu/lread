package com.lalilu.lread.fragment

import androidx.lifecycle.lifecycleScope
import com.lalilu.lread.BR
import com.lalilu.lread.EpubShower
import com.lalilu.lread.R
import com.lalilu.lread.adapter.WebViewPagerAdapter
import com.lalilu.lread.base.BaseFragment
import com.lalilu.lread.base.DataBindingConfig
import com.lalilu.lread.state.ReadViewModel
import com.lalilu.lread.state.SharedViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ReadFragment : BaseFragment() {
    private lateinit var mState: ReadViewModel
    private lateinit var mEvent: SharedViewModel

    override fun initViewModel() {
        mState = getFragmentViewModel(ReadViewModel::class.java)
        mEvent = getApplicationViewModel(SharedViewModel::class.java)
    }

    override fun getDataBindingConfig(): DataBindingConfig {
//        BookLoadingDialog.showDialog(parentFragmentManager, "LOADING")
//        val webViewClient = object : WebViewClient() {
//
//            override fun onPageFinished(view: WebView?, url: String?) {
//                BookLoadingDialog.postHideDialog()
//            }
//        }

        val adapter = WebViewPagerAdapter()
        return DataBindingConfig(R.layout.fragment_read, BR.vm, mState)
            .addParam(BR.view_pager_adapter, adapter)
//            .addParam(BR.webViewClient, webViewClient)
    }

    override fun loadInitData() {
        lifecycleScope.launch(Dispatchers.IO) {
            val lBook = BookListFragmentArgs.fromBundle(requireArguments()).lbook
            EpubShower.getInstance(requireContext()).load(lBook)
            mState.lBook.postValue(lBook)
        }
    }
}