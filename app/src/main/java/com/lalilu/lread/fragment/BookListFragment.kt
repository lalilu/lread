package com.lalilu.lread.fragment

import android.view.View
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.navigation.fragment.findNavController
import com.lalilu.lread.BR
import com.lalilu.lread.EpubScanner
import com.lalilu.lread.R
import com.lalilu.lread.adapter.LBookAdapter
import com.lalilu.lread.base.BaseFragment
import com.lalilu.lread.base.DataBindingConfig
import com.lalilu.lread.databinding.EmptyViewTipsBinding
import com.lalilu.lread.databinding.ItemAddLbookBinding
import com.lalilu.lread.entity.LBook
import com.lalilu.lread.state.BookListViewModel
import com.lalilu.lread.state.SharedViewModel
import com.lalilu.lread.utils.AlbumItemAnimator
import com.lalilu.lread.utils.ObjectUtils

class BookListFragment : BaseFragment() {
    private lateinit var mState: BookListViewModel
    private lateinit var mEvent: SharedViewModel
    private lateinit var adapter: LBookAdapter

    private lateinit var openDocumentsLauncher: ActivityResultLauncher<Array<String>>
    private val epubMime = arrayOf("application/epub+zip")
    private val addBookListener = View.OnClickListener { openDocumentsLauncher.launch(epubMime) }

    override fun initViewModel() {
        mState = getFragmentViewModel(BookListViewModel::class.java)
        mEvent = getApplicationViewModel(SharedViewModel::class.java)
    }

    override fun getDataBindingConfig(): DataBindingConfig {
        openDocumentsLauncher =
            registerForActivityResult(ActivityResultContracts.OpenMultipleDocuments()) { list ->
                EpubScanner.getInstance(requireActivity()).scanFromUrisByEpubLib(list) {
                    mEvent.bookList.requireData()
                }
            }

        adapter = LBookAdapter().also {
            it.setOnItemClickListener { adapter, _, position ->
                if (ObjectUtils.isNotSafe(adapter.data[position], LBook::class.java)) {
                    return@setOnItemClickListener
                }
                val bundle =
                    BookListFragmentArgs.Builder(adapter.data[position] as LBook).build().toBundle()

                findNavController().navigate(R.id.action_bookListFragment_to_readFragment, bundle)
            }
            val emptyView = EmptyViewTipsBinding.inflate(layoutInflater).also { binding ->
                binding.onClickListener = addBookListener
            }
            val headerView = ItemAddLbookBinding.inflate(layoutInflater).also { binding ->
                binding.onClickListener = addBookListener
            }
            it.setEmptyView(emptyView.root)
            it.setHeaderView(headerView.root, 0)
        }

        return DataBindingConfig(R.layout.fragment_book_list, BR.vm, mState)
            .addParam(BR.itemAnimator, AlbumItemAnimator())
            .addParam(BR.addBookClickListener, addBookListener)
            .addParam(BR.adapter, adapter)
    }

    override fun loadInitData() {
        mEvent.bookList.getData().observe(this.viewLifecycleOwner) {
            mState.bookList.postValue(it)
        }
    }
}