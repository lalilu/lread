package com.lalilu.lread.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class LBook(
    var id: String,
    var title: String? = null,
    var authors: String? = null,
    var publishers: String? = null,
    var contributors: String? = null,
    var publishDate: Date? = null,
    var descriptions: String? = null,
    var identifier: String? = null,
    var language: String? = null,
    var pageCount: Int = 0,
    var coverUriString: String? = null,
    var uriString: String? = null,
) : Parcelable
