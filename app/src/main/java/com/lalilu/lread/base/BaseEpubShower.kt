package com.lalilu.lread.base

interface BaseEpubShower<Target, Item> {
    var source: Item?

    fun load(item: Item): BaseEpubShower<Target, Item>
    fun select(pageNum: Int): BaseEpubShower<Target, Item>
    fun selectDocument(pageNum: Int): String?

    fun parsePath(data: String): String
    fun showAt(target: Target)
}