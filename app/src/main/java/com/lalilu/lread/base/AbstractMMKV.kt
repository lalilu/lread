package com.lalilu.lread.base

import android.os.Parcelable
import com.tencent.mmkv.MMKV

/**
 * 实现getAllItem方法的MMKV抽象容器
 */
abstract class AbstractMMKV<Key, Item : Parcelable>(mmkvKey: String) : BaseMMKV<Key, Item> {
    protected val mmkv: MMKV = MMKV.mmkvWithID(mmkvKey)

    inline fun <reified Item : Parcelable> getAllItem(): List<Item>? {
        return safeMmkv.allKeys()?.mapNotNull {
            safeMmkv.decodeParcelable(it, Item::class.java)
        }
    }

    @PublishedApi
    internal val safeMmkv: MMKV
        get() = mmkv
}