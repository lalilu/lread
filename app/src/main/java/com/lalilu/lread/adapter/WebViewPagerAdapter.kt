package com.lalilu.lread.adapter

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.lalilu.lread.EpubShower
import com.lalilu.lread.R
import com.lalilu.lread.databinding.ItemSinglePageBinding

class WebViewPagerAdapter : BaseQuickAdapter<Int, BaseViewHolder>(R.layout.item_single_page) {
    private val epubShower = EpubShower.getInstance(null)

    init {
        setDiffCallback(DiffInt())
    }

    override fun convert(holder: BaseViewHolder, item: Int) {
        println("loading item: $item")

        val binding = DataBindingUtil.bind(holder.itemView)
            ?: ItemSinglePageBinding.bind(holder.itemView)

        epubShower.select(item).showAt(binding.webviewMain)
    }

    class DiffInt : DiffUtil.ItemCallback<Int>() {
        override fun areItemsTheSame(oldItem: Int, newItem: Int): Boolean = oldItem == newItem
        override fun areContentsTheSame(oldItem: Int, newItem: Int): Boolean = oldItem == newItem
    }
}