package com.lalilu.lread.adapter

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.lalilu.lread.R
import com.lalilu.lread.databinding.ItemLbookBinding
import com.lalilu.lread.entity.LBook

class LBookAdapter :
    BaseQuickAdapter<LBook, BaseViewHolder>(R.layout.item_lbook) {

    init {
        setDiffCallback(DiffLBook())
    }

    override fun convert(holder: BaseViewHolder, item: LBook) {
        val binding = DataBindingUtil.bind(holder.itemView)
            ?: ItemLbookBinding.bind(holder.itemView)
        binding.lBook = item
    }

    class DiffLBook : DiffUtil.ItemCallback<LBook>() {
        override fun areItemsTheSame(oldItem: LBook, newItem: LBook): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: LBook, newItem: LBook): Boolean {
            return oldItem.id == newItem.id &&
                    oldItem.title == newItem.title
        }
    }
}