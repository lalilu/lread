package com.lalilu.lread

import com.facebook.drawee.backends.pipeline.Fresco
import com.hjq.permissions.XXPermissions
import com.lalilu.lread.base.BaseApplication
import com.tencent.mmkv.MMKV

class MainApp : BaseApplication() {

    override fun onCreate() {
        super.onCreate()
        MMKV.initialize(this)
        GlobalConfig.getInstance(this)
        EpubMMKV.getInstance(this)
        EpubScanner.getInstance(this)
        EpubShower.getInstance(this)
        XXPermissions.setScopedStorage(true)
        Fresco.initialize(this)
    }

}