package com.lalilu.lread.utils

object ObjectUtils {
    inline fun <reified T> isSafe(data: T, clazz: Class<*>): Boolean {
        return data != null && T::class.java == clazz
    }

    inline fun <reified T> isNotSafe(data: T, clazz: Class<*>): Boolean {
        return data == null && T::class.java != clazz
    }
}