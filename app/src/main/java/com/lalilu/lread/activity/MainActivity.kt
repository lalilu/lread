package com.lalilu.lread.activity

import android.content.Context
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.library.baseAdapters.BR
import com.hjq.permissions.OnPermissionCallback
import com.hjq.permissions.Permission
import com.hjq.permissions.XXPermissions
import com.lalilu.lread.R
import com.lalilu.lread.base.BaseActivity
import com.lalilu.lread.base.DataBindingConfig
import com.lalilu.lread.state.MainActivityViewModel
import com.lalilu.lread.state.SharedViewModel
import com.lalilu.lread.utils.StatusBarUtil

class MainActivity : BaseActivity() {
    private lateinit var mState: MainActivityViewModel
    private lateinit var mEvent: SharedViewModel

    override fun initViewModel() {
        mState = getActivityViewModel(MainActivityViewModel::class.java)
        mEvent = getApplicationViewModel(SharedViewModel::class.java)
    }

    override fun getDataBindingConfig(): DataBindingConfig {
        return DataBindingConfig(R.layout.activity_main, BR.vm, mState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        StatusBarUtil.setStatusBarColor(this)
        mEvent.bookList.requireData()
//        checkReadExternalStoragePermission(this)
    }

    private fun checkReadExternalStoragePermission(context: Context): Boolean {
        if (XXPermissions.isGranted(context, Permission.READ_EXTERNAL_STORAGE)) {
            return true
        }
        var result = false
        XXPermissions.with(context).permission(Permission.READ_EXTERNAL_STORAGE)
            .request(object : OnPermissionCallback {
                override fun onGranted(permissions: MutableList<String>?, all: Boolean) {
                    result = true
                }

                override fun onDenied(permissions: MutableList<String>?, never: Boolean) {
                    val notice = if (never) "已被永久拒绝授权，请前往设置进行开启" else "获取外部读写授权失败"
                    Toast.makeText(context, notice, Toast.LENGTH_SHORT).show()
                }
            })
        return result
    }
}