package com.lalilu.lread.request

import com.lalilu.lread.EpubMMKV
import com.lalilu.lread.base.BaseRequest
import com.lalilu.lread.entity.LBook


class BookListRequest : BaseRequest<List<LBook>>() {
    private val epubMMKV: EpubMMKV = EpubMMKV.getInstance(null)

    /**
     * 同步
     */
    override fun requestData() {
        setData(epubMMKV.getAll())
    }

    /**
     * 异步
     */
    override fun requireData() {
        postData(epubMMKV.getAll())
    }
}